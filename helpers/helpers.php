<?php

use Illuminate\Support\Facades\File;

if (!function_exists('loipham_path')) {
    /**
     * @param string|null $path
     * @return string
     */
    function loipham_path(string $path = null): string {
        if (File::exists(base_path('loipham/' . $path))) {
            $resultPath = base_path('loipham/' . $path);
        } else {
            $resultPath = base_path('vendor/loipham/' . $path);
        }
        return $resultPath;
    }
}

if (!function_exists('woocommerce_path')) {
    /**
     * @param string|null $path
     * @return string
     */
    function woocommerce_path(string $path = null): string {
        return loipham_path('woocommerce/' . $path);
    }
}

if (!function_exists('scan_folder')) {
    /**
     * @param string $path
     * @param array $ignoreFiles
     * @return array
     * @author Loi Pham
     */
    function scan_folder(string $path, array $ignoreFiles = []): array {
        try {
            if (File::isDirectory($path)) {
                $data = array_diff(scandir($path), array_merge(['.', '..', '.DS_Store'], $ignoreFiles));
                natsort($data);
                return $data;
            }
            return [];
        } catch (Exception $exception) {
            return [];
        }
    }
}

