<?php
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

//Route::any('', function () {
//    return File::get(public_path() . '\landing\dist\index.html');
//})->where("path", ".+");

$namespaceBackEnd = "\LoiPham\WooCommerce\App\Modules\BackEnd";
$namespaceFontEnd = "\LoiPham\WooCommerce\App\Modules\FontEnd";
Route::group(['namespace' => $namespaceBackEnd], function () {
    Route::post('login', 'Auth\Controllers\AuthController@login');
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::post('logout', 'Auth\Controllers\AuthController@logout');
        Route::group(['prefix' => 'product'], function () {
            Route::get('list', 'Product\Controllers\ProductController@index');
            Route::get('all-options', 'Product\Controllers\ProductController@allOptions');
        });
    });
});
