<?php

    return [
        'admin_dir' => env('ADMIN_DIR', 'admin'),
        'base_name' => env('APP_NAME', 'WooCommerce Management'),
        'admin_url' => [
            'language' => 'language',
            'product' => 'product',
            'media' => 'media-management',
            'setting' => 'setting',
        ],
        'cache_admin_menu_enable' => true
    ];
