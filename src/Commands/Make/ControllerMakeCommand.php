<?php

namespace LoiPham\WooCommerce\Commands\Make;

use Illuminate\Contracts\Container\BindingResolutionException;
use League\Flysystem\FileNotFoundException;
use LoiPham\WooCommerce\Commands\Abstracts\BaseMakeCommand;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class ControllerMakeCommand extends BaseMakeCommand
{
    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'woo:make:controller {name : The controller that you want to create}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a controller';

    /**
     * Execute the console command.
     * @throws FileNotFoundException
     * @author Loi Pham
     */
    public function handle(): bool
    {
//        if (!preg_match('/^[a-z0-9\-\_]+$/i', $this->argument('name'))) {
//            $this->error('Only alphabetic characters are allowed.');
//            return false;
//        }
        $name = $this->argument('name');

        $path = woocommerce_path('src/App/Modules/' . ucfirst(Str::studly($name)) . 'Controller.php');
        $this->publishStubs($this->getStub(), $path);
        $this->renameFiles($name, $path);
        $this->searchAndReplaceInFiles($name, $path);
        $this->line('------------------');
        $this->info('Đã tạo thành công controller tại ' . $path . '');
        return true;
    }

    /**
     * @return string
     */
    public function getStub($name = null): string
    {
        $wooCommerceControllerStubPath = 'loipham' . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'Controller.stub';
        if (!base_path($wooCommerceControllerStubPath)) {
            $wooCommerceControllerStubPath = 'vendor' . DIRECTORY_SEPARATOR . $wooCommerceControllerStubPath;
        }
        return base_path($wooCommerceControllerStubPath);
    }

    /**
     * @param string $replaceText
     * @return array
     */
    public function getReplacements(string $replaceText): array
    {
        $explodeControllerName = explode('/', $replaceText);
        $controllerName = $explodeControllerName[count($explodeControllerName) - 1];
        $nameSpace = $this->getNameSpace($explodeControllerName);

        return [
            '{NameSpace}' => $nameSpace,
            '{-name}' => strtolower($controllerName),
            '{name}' => Str::snake(str_replace('-', '_', $controllerName)),
            '{+name}' => Str::camel($controllerName),
            '{names}' => Str::plural(Str::snake(str_replace('-', '_', $controllerName))),
            '{-names}' => Str::plural($controllerName),
            '{NAME}' => strtoupper(Str::snake(str_replace('-', '_', $controllerName))),
            '{Name}' => ucfirst(Str::camel($controllerName)),
            '.stub' => '.php'
        ];
    }

    private function getNameSpace($explodeControllerName): ?string
    {
        $result = null;
        unset($explodeControllerName[count($explodeControllerName) - 1]);
        foreach($explodeControllerName as $nameSpace) {
            $result .= '\\' . $nameSpace;
        }
        return $result;
    }
}
