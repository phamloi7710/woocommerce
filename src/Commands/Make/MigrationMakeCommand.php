<?php

namespace LoiPham\WooCommerce\Commands\Make;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Date;
use League\Flysystem\FileNotFoundException;
use LoiPham\WooCommerce\Commands\Abstracts\BaseMakeCommand;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Carbon\Carbon;

class MigrationMakeCommand extends BaseMakeCommand
{
    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'woo:make:migration {name : The migration that you want to create}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a migration';

    /**
     * Execute the console command.
     * @throws FileNotFoundException
     * @author Loi Pham
     */
    public function handle(): bool
    {
        $name = Str::snake(trim($this->input->getArgument('name')));
        $path = woocommerce_path('src/Databases/Migrations/' . Carbon::now()->format('Y_m_d_His_') . $name . '.php');
        $this->publishStubs($this->getStub($name), $path);
        $this->renameFiles($name, $path);
        $this->searchAndReplaceInFiles($name, $path);
        $this->line('------------------');
        $this->info('Đã tạo thành công migration tại ' . $path . '');
        return true;
    }

    /**
     * @return string
     */
    public function getStub($name = null): string
    {
        $explodeMigrationName = explode('_', $name);
        if ($explodeMigrationName[0] != 'create') {
            $wooCommerceMigrationStubPath = 'loipham' . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'migration' . DIRECTORY_SEPARATOR . 'MigrationUpdateTable.stub';
        } else {
            $wooCommerceMigrationStubPath = 'loipham' . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'migration' . DIRECTORY_SEPARATOR . 'MigrationCreateTable.stub';
        }

        if (!base_path($wooCommerceMigrationStubPath)) {
            $wooCommerceMigrationStubPath = 'vendor' . DIRECTORY_SEPARATOR . $wooCommerceMigrationStubPath;
        }
        return base_path($wooCommerceMigrationStubPath);
    }

    /**
     * @param string $replaceText
     * @return array
     */
    public function getReplacements(string $replaceText): array
    {
        $explodeMigrationName = explode('_', $replaceText);
        $tableName = $explodeMigrationName[count($explodeMigrationName) - 2];
        $migrationName = ucfirst(Str::camel($replaceText));
        return [
            '{TableName}' => $tableName,
            '{MigrationName}' => ucfirst(Str::camel($migrationName)),
            '.stub' => '.php'
        ];
    }
}
