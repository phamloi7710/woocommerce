<?php

namespace LoiPham\WooCommerce\Commands;

use LoiPham\WooCommerce\App\Modules\Admin\User\Models\User;
use LoiPham\WooCommerce\Exceptions\MakeUserException;
use Illuminate\Console\Command;

class CreateSampleDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'woocommerce:make:data-sample';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create data sample for application';

    /**
     * Execute the console command.
     *
     * Handle creation of the new application user.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('db:seed');
    }

    /**
     * Determine if the given email address already exists.
     *
     * @param string $email
     * @return void
     *
     * @throws \Dyrynda\Artisan\Exceptions\MakeUserException
     */
    private function validateEmail(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw MakeUserException::invalidEmail($email);
        }
        if (app(config('auth.providers.users.model'))->where('email', $email)->exists()) {
            throw MakeUserException::emailExists($email);
        }
    }
}
