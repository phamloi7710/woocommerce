<?php

namespace LoiPham\WooCommerce\Commands\Abstracts;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use League\Flysystem\Adapter\Local as LocalAdapter;
use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\MountManager;

abstract class BaseMakeCommand extends Command
{
    /**
     * Kiểm tra thư mục upload
     * Nếu chưa có thì hệ thống sẽ tạo thư mục
     */
    public function checkFolder($pathExploded)
    {
        $path = null;
        if (count($pathExploded) > 0) {
            foreach ($pathExploded as $value) {
                $path .= $value . '\\';
            }
        }

        $folders = explode(DS, $path);

        $currentFolder = '';
        foreach ($folders as $folder) {
            if (!empty($currentFolder))
                $currentFolder .= DS;
            $currentFolder .= $folder;
            if (!file_exists($currentFolder)) {
                File::makeDirectory($currentFolder);
            }
        }
    }

    /**
     * Generate the module in Modules directory.
     * @param string $from
     * @param string $to
     * @throws \League\Flysystem\FileNotFoundException
     * @author Loi Pham
     */
    protected function publishStubs(string $from, string $to)
    {
        $explodeControllerName = explode('/', $to);
        unset($explodeControllerName[count($explodeControllerName) - 1]);
        $this->checkFolder($explodeControllerName);
        if (File::isDirectory($from)) {
            $this->publishDirectory($from, $to);
        } else {
            File::copy($from, $to);
        }
    }

    /**
     * Search and replace all occurrences of ‘Module’
     * in all files with the name of the new module.
     * @param string $patern
     * @param string $location
     * @param null $stub
     * @return bool
     * @throws \League\Flysystem\FileNotFoundException
     * @author Loi Pham
     */
    public function searchAndReplaceInFiles(string $patern, string $location, $stub = null): bool
    {
        $replacements = $this->replacements($patern);
        if (File::isFile($location)) {
            if (!$stub) {
                $stub = File::get($this->getStub());
            }

            $replace = $this->getReplacements($patern) + $this->baseReplacements($patern);

            $content = str_replace(array_keys($replace), $replace, $stub);
//            File::makeDirectory($path)
            File::put($location, $content);
            return true;
        }

        $manager = new MountManager([
            'directory' => new Flysystem(new LocalAdapter($location)),
        ]);

        foreach ($manager->listContents('directory://', true) as $file) {
            if ($file['type'] === 'file') {
                $content = str_replace(
                    array_keys($replacements),
                    array_values($replacements),
                    $manager->read('directory://' . $file['path'])
                );

                $manager->put('directory://' . $file['path'], $content);
            }
        }

        return true;
    }

    /**
     * Rename models and repositories.
     * @param $pattern
     * @param string $location
     * @return boolean
     * @author Loi Pham
     */
    public function renameFiles($pattern, string $location): bool
    {
        $paths = scan_folder($location);

        if (empty($paths)) {
            return false;
        }

        foreach ($paths as $path) {
            $path = $location . DIRECTORY_SEPARATOR . $path;

            $newPath = $this->transformFileName($pattern, $path);
            rename($path, $newPath);

            $this->renameFiles($pattern, $newPath);
        }

        return true;
    }

    /**
     * Rename file in path.
     *
     * @param string $pattern
     * @param string $path
     * @return string
     * @author Loi Pham
     */
    public function transformFileName(string $pattern, string $path): string
    {
        $replacements = $this->replacements($pattern);

        return str_replace(
            array_keys($replacements),
            array_values($replacements),
            $path
        );
    }

    /**
     * Publish the directory to the given directory.
     *
     * @param string $from
     * @param string $to
     * @return void
     * @throws \League\Flysystem\FileNotFoundException
     * @author Loi Pham
     */
    protected function publishDirectory($from, $to)
    {
        $manager = new MountManager([
            'from' => new Flysystem(new LocalAdapter($from)),
            'to' => new Flysystem(new LocalAdapter($to)),
        ]);

        foreach ($manager->listContents('from://', true) as $file) {
            if ($file['type'] === 'file' && (!$manager->has('to://' . $file['path']) || $this->option('force'))) {
                $manager->put('to://' . $file['path'], $manager->read('from://' . $file['path']));
            }
        }
    }

    /**
     * Create the directory to house the published files if needed.
     *
     * @param string $path
     * @return void
     * @author Loi Pham
     */
    protected function createParentDirectory(string $path)
    {
        if (!File::isDirectory($path) && !File::isFile($path)) {
            File::makeDirectory($path, 0755, true);
        }
    }

    /**
     * @param string $replaceText
     * @return array
     */
    public function baseReplacements(string $replaceText): array
    {
        return [
            '{-plugin}' => strtolower($replaceText),
            '{plugin}' => Str::snake(str_replace('-', '_', $replaceText)),
            '{+plugin}' => Str::camel($replaceText),
            '{plugins}' => Str::plural(Str::snake(str_replace('-', '_', $replaceText))),
            '{-plugins}' => Str::plural($replaceText),
            '{PLUGIN}' => strtoupper(Str::snake(str_replace('-', '_', $replaceText))),
            '{Plugin}' => ucfirst(Str::camel($replaceText)),
            '.stub' => '.php',
            '{migrate_date}' => now(config('app.timezone'))->format('Y_m_d_His'),
        ];
    }

    /**
     * @param string $replaceText
     * @return array
     */
    public function replacements(string $replaceText): array
    {
        return array_merge($this->getReplacements($replaceText), $this->baseReplacements($replaceText));
    }

    /**
     * @param string $replaceText
     * @return array
     */
    abstract public function getReplacements(string $replaceText): array;

    /**
     * @return string
     */
    abstract public function getStub($name = null): string;


    /**
     * Determine if the given path(s) are pre-resolved "real" paths.
     *
     * @return bool
     */
    protected function usingRealPath(): bool
    {
        return $this->input->hasOption('realpath') && $this->option('realpath');
    }

    /**
     * Get the path to the migration directory.
     *
     * @return string
     */
    protected function getMigrationPath(): string
    {
        return $this->laravel->databasePath().DIRECTORY_SEPARATOR.'migrations';
    }






    /**
     * Get all of the migration paths.
     *
     * @return array
     */
    protected function getMigrationPaths(): array
    {
        // Here, we will check to see if a path option has been defined. If it has we will
        // use the path relative to the root of the installation folder so our database
        // migrations may be run for any customized path from within the application.
        if ($this->input->hasOption('path') && $this->option('path')) {
            return collect($this->option('path'))->map(function ($path) {
                return ! $this->usingRealPath()
                    ? $this->laravel->basePath().'/'.$path
                    : $path;
            })->all();
        }

        return array_merge(
            $this->migrator->paths(), [$this->getMigrationPath()]
        );
    }

}
