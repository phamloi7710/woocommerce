<?php

namespace LoiPham\WooCommerce\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User;
use Illuminate\Support\Facades\Config;
use LoiPham\WooCommerce\Commands\CreateSuperUserCommand;

class SetupCommand extends Command
{
    /**
     * @var CreateSuperUserCommand
     */
    protected $createSuperUserCommand;

    /**
     * @var CreateSuperUserCommand
     */
    protected $createSampleDataCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $file;
    protected $signature = 'woocommerce:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup the WooCommerce Management';

    /**
     *
     * Create a new command instance.
     *
     * @param \LoiPham\WooCommerce\Commands\CreateSuperUserCommand $createSuperUserCommand
     */
    public function __construct(
        CreateSuperUserCommand $createSuperUserCommand,
        CreateSampleDataCommand $createSampleDataCommand
    )
    {
        $this->file = app('files');
        $this->createSuperUserCommand = $createSuperUserCommand;
        $this->createSampleDataCommand = $createSampleDataCommand;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Đang tiến hành cài đặt...');
//        $this->file->copyDirectory('vendor/loipham/shopping/public/app-assets', 'public/app-assets');
        $this->call('migrate');
        if ($this->confirm('Bạn có muốn tạo khoản Admin không? [yes|no]')) {
            $this->call($this->createSuperUserCommand->getName());
        }

        $this->setConfig();

        $this->call('config:cache');
        $this->call('route:cache');
        if ($this->confirm('Bạn có muốn tạo dữ liệu mẫu không? [yes|no]')) {
            // Tiến hành tạo dữ liệu mẫu
            $this->call($this->createSampleDataCommand->getName());
        }

        $this->info('Quá trình cài đặt website bán hàng online đã hoàn tất');
    }

    protected function setConfig()
    {
        $auth_config_file = base_path('config/auth.php');
        $search = 'WooCommerce';
        if ($this->checkExist($auth_config_file, $search)) {
            $data =
"<?php
    return [
        'defaults' => [
            'guard' => 'admin',
            'passwords' => 'admin',
        ],
        'guards' => [
            'web' => [
                'driver' => 'session',
                'provider' => 'users',
            ],
            'admin' => [
                'driver' => 'sanctum',
                'provider' => 'admin',
            ],
        ],
        'providers' => [
            'users' => [
                'driver' => 'eloquent',
                'model' => \LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User::class,
            ],
            'admin' => [
                'driver' => 'eloquent',
                'model' => \LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User::class,
            ]
        ],
        'passwords' => [
            'users' => [
                'provider' => 'users',
                'table' => 'password_resets',
                'expire' => 60,
                'throttle' => 60,
            ],
            'admin' => [
                'provider' => 'admin',
                'table' => 'admin_password_resets',
                'expire' => 60,
                'throttle' => 60,
            ],
        ],
        'password_timeout' => 10800,

    ]; ";
        $this->file->put($auth_config_file, $data);
        }
    }

    /**
     * [checkExist description].
     *
     * @param $file
     * @param $search
     * @return bool [type] [description]
     * @author Loi Pham
     */
    protected function checkExist($file, $search): bool
    {
        return $this->file->exists($file) && !Str::contains($this->file->get($file), $search);
    }
}
