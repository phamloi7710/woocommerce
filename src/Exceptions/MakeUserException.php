<?php

namespace LoiPham\WooCommerce\Exceptions;

use InvalidArgumentException;

class MakeUserException extends InvalidArgumentException
{
    /**
     * The supplied email is invalid.
     *
     * @param string $email
     * @return MakeUserException
     */
    public static function invalidEmail(string $email): MakeUserException
    {
        return new static("The email address [{$email}] is invalid");
    }

    /**
     * The supplied email already exists.
     *
     * @param string $email
     * @return MakeUserException
     */
    public static function emailExists(string $email): MakeUserException
    {
        return new static("A user with the email address {$email} already exists");
    }
}
