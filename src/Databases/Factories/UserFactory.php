<?php

namespace LoiPham\WooCommerce\Databases\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    public function definition(): array
    {
        return [
            'code' => 'ADMIN00001',
            'name' => 'Phạm Văn Lợi',
            'username' => 'admin',
            'email' => 'phamloi7710@gmail.com',
            'password' => Hash::make('111111'),
            'phone_number' => '0963227710',
            'gender' => 'male',
            'birthday' => '1994-04-30',
            'default_user' => 'yes'
        ];
    }
}
