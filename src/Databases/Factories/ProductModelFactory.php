<?php

namespace LoiPham\WooCommerce\Databases\Factories;

use LoiPham\WooCommerce\App\Modules\BackEnd\Product\Models\ProductModel;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductModelFactory extends Factory
{
    protected $talble = 'products';
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sku' => Str::random(8),
            'name' => $this->faker->name(),
            'barcode' => Str::random(15),
            'created_by' => '1'
        ];
    }
}
