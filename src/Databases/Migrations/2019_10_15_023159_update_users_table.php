<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->string('code', 16)->after('id')->nullable();
            $table->string('username', 16)->after('name')->unique();
            $table->string('phone_number', 11)->after('password')->nullable();
            $table->enum('gender', ['male', 'female'])->after('phone_number')->default('male');
            $table->date('birthday')->after('gender')->nullable();
            $table->enum('status', ['active', 'inActive'])->after('birthday')->default('active');
            $table->enum('default_user', ['yes', 'no'])->after('status')->default('no');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
