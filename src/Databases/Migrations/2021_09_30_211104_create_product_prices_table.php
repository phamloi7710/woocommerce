<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id')->index()->references('id')->on('products');
            $table->decimal('retail_price', 20, 6)->default(0);
            $table->decimal('wholesale_price', 20, 6)->default(0);
            $table->unsignedBigInteger('created_by')->index()->references('id')->on('users');
            $table->timestamps();
            $table->unsignedBigInteger('deleted_by')->index()->references('id')->on('users');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
