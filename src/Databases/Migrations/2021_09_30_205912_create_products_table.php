<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 32)->unique()->nullable();
            $table->string('name', 128)->nullable();
            $table->string('barcode', 128)->unique()->nullable();
            $table->unsignedDecimal('weight', 20, 6)->nullable();
            $table->enum('status', ['active', 'inActive'])->default('active');
            $table->string('image', 128)->nullable();
            $table->mediumText('description')->nullable();
            $table->mediumText('note')->nullable();
            $table->string('private_code')->unique()->nullable();
            $table->unsignedBigInteger('created_by')->index()->references('id')->on('users');
            $table->timestamps();
            $table->unsignedBigInteger('deleted_by')->index()->references('id')->on('users')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
