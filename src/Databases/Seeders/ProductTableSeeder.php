<?php

namespace LoiPham\WooCommerce\Databases\Seeders;

use Illuminate\Database\Seeder;
use LoiPham\WooCommerce\App\Modules\BackEnd\Product\Models\ProductModel;

class ProductTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ProductModel::factory(1000)->create();
    }
}
