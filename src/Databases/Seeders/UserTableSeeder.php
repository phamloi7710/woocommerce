<?php

namespace LoiPham\WooCommerce\Databases\Seeders;

use Illuminate\Database\Seeder;
use LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create();
    }
}
