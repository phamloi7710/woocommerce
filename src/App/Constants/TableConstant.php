<?php

namespace LoiPham\WooCommerce\App\Constants;

final class TableConstant extends BaseConstant
{
    const USERS_TABLE = 'users';
    const BUILDINGS_TABLE = 'buildings';
}
