<?php

namespace LoiPham\WooCommerce\App\Constants;

define('ROOT', dirname(dirname(__FILE__)));
define('WooCommerce_PACKAGE_PATH', realpath(dirname(ROOT)));

final class PathConstant extends BaseConstant
{
    const SEEDER_PATH = WooCommerce_PACKAGE_PATH . DS . 'Databases' . DS . 'Seeders';
}
