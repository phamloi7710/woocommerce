<?php

namespace LoiPham\WooCommerce\App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;


class AppRequest extends FormRequest
{
    protected $responses = null;

    public function authorize(): bool
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(
            [
                'error' => true,
                'message' => __('form.form_invalid'),
                'status' => 422,
                'data' => $this->formatErrors($validator)
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)); // 422
    }

//    protected function formatErrors(Validator $validator): array
//    {
//        $errors = $validator->errors()->getMessages();
//        $obj = $validator->failed();
//        $result = [];
//        foreach($obj as $input => $rules){
//            $i = 0;
//            foreach($rules as $rule => $ruleInfo){
//                $rule = $input.'['.strtolower($rule).']';
//                $result[$rule] = $errors[$input][$i];
//                $i++;
//            }
//        }
//        return $result;
//    }


    protected function formatErrors(Validator $validator): array
    {
        $errors = $validator->errors()->getMessages();
        $obj = $validator->failed();
        $result = [];
        foreach ($obj as $input => $rules) {
            $i = 0;
            foreach ($rules as $rule => $ruleInfo) {
                $rule = strtolower($rule);
                $result[$input][$rule] = $errors[$input][$i];
                $i++;
            }
        }
        return $result;
    }
}
