<?php

namespace LoiPham\WooCommerce\App\Http\Requests\BackEnd\Auth;

use LoiPham\WooCommerce\App\Http\Requests\AppRequest;

class LoginRequest extends AppRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'username' => [
                'required',
                'min:3',
                'max:10'
            ],
            'password' => [
                'required',
                'min:3',
                'max:12'
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'username.required' => __('validation.required', [__('username')]),
            'username.min' => __('validation.min.string', [__('username'), 3]),
            'username.max' => __('validation.max.string', [__('username'), 10]),

            'password.required' => __('validation.required', [__('password')]),
            'password.min' => __('validation.min.string', [__('password'), 3]),
            'password.max' => __('validation.max.string', [__('password'), 12])
        ];
    }
}
