<?php

namespace LoiPham\WooCommerce\App\Http\Base\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use LoiPham\WooCommerce\App\Http\Responses\WooCommerceHttpResponse;
use LoiPham\WooCommerce\App\Http\Base\Models\Setting\SettingModel;

class ManagerController extends BaseController
{
    protected $data = [];
    protected $tableName = '';
    protected $indexTitle = '';
    protected $tableClass = '';
    protected $viewRender = '';
    protected $rules = [];
    protected $messages = [];
    protected $viewNewItem = null;
    protected $successMessage = null;
    protected $listQueryParams = [];
    protected $joinConditions = [];

    /**
     * @throws Exception
     */
//    public function indexTest(Request $request): JsonResponse
//    {
//        try {
//            $table = $this->getTable();
//            $table->from($this->tableName, $this->getTableAlias());
//            $querySearch = $request->get('search');
//            $querySort = $request->get('sort');
//            $currentPage = $request->get('page');
//            $limit = $request->get('limit', 20);
//            $search = ($querySearch) ? json_decode(urldecode(base64_decode($querySearch))) : null;
//            $sorts = ($querySort) ? json_decode(urldecode(base64_decode($querySort))) : null;
//
////            if ($search != null) {
////                foreach ($search as $valueSearch) {
////                    if (!empty($valueSearch->compare)) {
////                        switch ($valueSearch->compare) {
////                            case 'not_in':
////                                $table->whereNotIn($valueSearch->column, $valueSearch->value);
////                                break;
////                            case 'in':
////                                $table->whereIn($valueSearch->column, $valueSearch->value);
////                                break;
////                            case 'like':
////                                $table->where($valueSearch->column, $valueSearch->compare, '%' . $valueSearch->value . '%');
////                                break;
////                            default:
////                                if (!is_array($valueSearch->value)) {
////                                    $table->where($valueSearch->column, $valueSearch->compare, $valueSearch->value);
////                                }
////                                break;
////                        }
////                    }
////                }
////            }
////
////            if ($sorts != null) {
////                if (count((array)$sorts) > 0) {
////                    foreach ((array)$sorts as $key => $sort) {
////                        $table->orderBy($key, $sort);
////                    }
////                }
////            }
////
////            if (count($this->joinConditions) > 0) {
////                foreach ($this->joinConditions as $joinCondition) {
////                    switch ($joinCondition[5]) {
////                        case 'left':
//////                            $table->leftJoin($this->getTableNameByTableClass($joinCondition[0]), $this->getTableAlias($joinCondition[0]) . '.' . $joinCondition[1], '=', $this->getTableAlias($joinCondition[2]) . '.' . $joinCondition[3]);
//////                            $table->select($this->getTableAlias() . '.*', $this->getColumnsInJoin($joinCondition[0], $joinCondition[4]));
////                            break;
////                    }
////                }
////            }
//
//        $this->data = $table->paginate($limit, ['*'], 'page', $currentPage)->withQueryString();
//        return $this->setResponse($this->data);
//        } catch (\ErrorException $error) {
//            throw new \Exception($error);
//        }
//    }

    /**
     * @throws Exception
     */
    public function index(Request $request): JsonResponse
    {
        try {
            $table = new $this->tableClass;
            $limit = $request->get('limit', 20);
            $currentPage = $request->get('page');
            $this->setListQueryParams($request);
            $table->setListQueryParams($this->listQueryParams);
            $result = $table->listItems();
            $this->data = $result->paginate($limit, ['*'], 'page', $currentPage)->withQueryString();
            return $this->setResponse($this->data);

        } catch (\ErrorException $error) {
            throw new \Exception($error);
        }
    }

    public function setListQueryParams($request) { }

//    protected function setOrderList($request)
//    {
//        $sort = $request->get('sort');
//
//        if ($sort) {
//            $sort = json_decode(urldecode(base64_decode($sort)));
//            if (isset($sort->column)) {
//                $this->listQueryParams['orders'] = [
//                    $sort->column => ($sort->direction) ? $sort->direction : 'asc'
//                ];
//            }
//        }
//    }

    public function getColumnsInJoin($tableClass, $columns): ?string
    {
        $result = null;
        if (!empty($columns) && count($columns) > 0) {
            foreach ($columns as $key => $column) {
                $result .= $this->getTableAliasByTableClass($tableClass) . '.' . $key . ' AS ' . $column;
            }
        }
        return $result;
    }

    public function getTableAlias($tableClass = null)
    {
        if (empty($tableClass)) {
            $table = new $this->tableClass;
        } else {
            $table = new $tableClass;
        }
        return $table->alias;
    }

    public function getTableAliasByTableClass($tableClass = null)
    {
        if (empty($tableClass)) {
            $table = new $this->tableClass;
        } else {
            $table = new $tableClass;
        }
        return $table->alias;
    }

    public function getTableNameByTableClass($tableClass = null): string
    {
        if (empty($tableClass)) {
            $table = new $this->tableClass;
        } else {
            $table = new $tableClass;
        }
        return $table->table . ' AS ' . $this->getTableAliasByTableClass($tableClass);
    }

    public function getTable($tableName = ''): ?\Illuminate\Database\Query\Builder
    {
        if (empty($tableName)) {
            return DB::table($this->tableName);
        } else {
            return DB::table($tableName);
        }
    }

    public function setListParams()
    {

    }

    public function loadConfigs(): JsonResponse
    {
        $settings = SettingModel::all();
        return response()->json($settings);
    }

    public function setFormValidators($rules, $messages, $request): ?JsonResponse
    {
        if ($rules != null && $messages != null) {
            $validator = Validator::make((array)$this->getApiRequestData($request), $rules, $messages);
            if ($validator->fails()) {
                return response()->json([
                    'error' => true,
                    'messages' => $validator->getMessageBag()->toArray()
                ], 400);
            }
            return response()->json([
                'error' => false,
                'messages' => null
            ], 200);
        }
        return null;
    }

    /**
     * Store action
     * @param Request $request
     * @return JsonResponse|WooCommerceHttpResponse|null
     */
    public function store(Request $request)
    {
        try {
            $this->data = (array)$this->getApiRequestData($request);
            $errors = $this->setFormValidators($this->rules, $this->messages, $request);
            if (!empty($errors) && $errors->original['error']) {
                return $errors;
            }
            $this->beforeStoreData();
            $result = $this->tableClassName::create($this->data);
            if (!$result['error']) {
                $this->afterStoreData();
                $id = $result['id'];
                $item = $this->tableClassName::findOrFail($id);
                return $this->setApiResponseSuccess($this->successMessage, view($this->viewNewItem, ['item' => $item])->render());
            }
        } catch (Exception $ex) {
            return $this->setApiResponseError($ex);
        }
    }

    public function update(Request $request, $data = [], $itemId = null)
    {
        try {
            $this->data = (array)$this->getApiRequestData($request);
            $errors = $this->setFormValidators($this->rules, $this->messages, $request);
            if (!empty($errors) && $errors->original['error']) {
                return $errors;
            }
//            $this->beforeStoreData();
            $result = $this->tableClassName::findOrFail($itemId)->update($this->data);

            if (!$result['error']) {
//                $this->afterStoreData();
                $id = $result['id'];
                $item = $this->tableClassName::findOrFail($id);
                return $this->setApiResponseSuccess($this->successMessage, view($this->viewNewItem, ['item' => $item])->render());
            }
        } catch (Exception $ex) {
            return $this->setApiResponseError($ex);
        }
    }

    /**
     * Store action
     * @param Request $request
     * @return JsonResponse|WooCommerceHttpResponse|null
     */
    public function storeAjax(Request $request)
    {
        try {
            $this->data = $request->all();
            $errors = $this->setFormValidators($this->rules, $this->messages, $request);
            if (!empty($errors) && $errors->original['error']) {
                return $errors;
            }
            $this->beforeStoreData();
            $result = $this->tableClass::create($this->data);
            if (!$result['error']) {
                $this->afterStoreData();
                $id = $result['id'];
                $error = $result['error'];
                $messages = $result['messages'];
                return response()->json([
                    'data' => $this->tableClass::findOrFail($id),
                    'id' => $id,
                    'error' => $error,
                    'messages' => $messages
                ]);
            }
        } catch (Exception $ex) {
            return $this->setApiResponseError($ex);
        }
    }


    /**
     * Update order of item when using sortable plugin of jquery
     * @param Request $request
     * @return JsonResponse | WooCommerceHttpResponse
     */
    public function updateOrderItem(Request $request)
    {
        try {
            $data = $this->tableClass::all();
            foreach ($data as $item) {
                foreach ($request->order as $order) {
                    if ($order['id'] == $item->id) {
                        $item->update(['order' => $order['position']]);
                    }
                }
            }
            return $this->setResponse(['error' => false, 'message' => null]);
        } catch (Exception $ex) {
            return $this->response
                ->setError()
                ->setMessage($ex->getMessage());
        }
    }

    /**
     * beforeStoreData
     */
    public function beforeStoreData()
    {
    }

    /**
     * afterStoreData
     */
    public function afterStoreData()
    {
    }

    public function newCode($key, $tableClass, $field_code = 'code'): string
    {
        //get prefix
        $prefix = '';
        $item = DB::table(TABLE_CODE_PREFIX)
            ->where('key', $key)->first();
        if ($item) {
            $prefix = $item->value;
        }

        //Lấy max code
        $currentTable = DB::table($tableClass::TABLE_NAME)
            ->select(DB::raw($field_code))->first();

        if ($currentTable) {
            $code = (int)str_replace($prefix, '', $currentTable->$field_code);
            if ($code == 0) {
                $code = 1;
            } else {
                $code += 1;
            }
        } else {
            $code = 1;
        }
        $code = $this->appendChar($code);
        return $prefix . $code;
    }

    public function appendChar($value, $len = 5, $char = '0'): string
    {
        while (strlen($value) < $len) {
            $value = $char . $value;
        }
        return $value;
    }
}
