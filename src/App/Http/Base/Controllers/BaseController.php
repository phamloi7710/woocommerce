<?php

namespace LoiPham\WooCommerce\App\Http\Base\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use LoiPham\WooCommerce\App\Http\Responses\WooCommerceHttpResponse;

class BaseController extends Controller
{
    protected $responses = null;
    protected $ipAddress = null;

    /**
     * LanguageController constructor.
     * @param WooCommerceHttpResponse $responses
     */
    public function __construct(WooCommerceHttpResponse $responses)
    {
        $this->responses = $responses;
    }

    public function setResponse($response): JsonResponse
    {
        return response()->json($response);
    }

    public function getApiRequestData($request)
    {
        return json_decode(json_encode($request->all()));
    }

    /**
     * @param null $data
     * @param null $message
     * @param int $statusCode
     * @return WooCommerceHttpResponse
     */
    public function setApiResponseSuccess($data = null, $message = null, int $statusCode = 200): WooCommerceHttpResponse
    {
        return $this->responses
            ->setCode($statusCode)
            ->setError(false)
            ->setMessage($message)
            ->setData($data);
    }

    /**
     * @param $response
     * @return WooCommerceHttpResponse
     */
    public function setApiResponse($response): WooCommerceHttpResponse
    {
//        $data = null, $message = null, int $statusCode = 401, bool $error = true
        return $this->responses
            ->setCode($response['code'] ?: 401)
            ->setError($response['error'] ?: false)
            ->setMessage($response['message'] ?: null)
            ->setData($response['data'] ?: null);
    }

    protected function setApiResponseError($e): WooCommerceHttpResponse
    {
        return $this->responses
            ->setCode(JsonResponse::HTTP_BAD_REQUEST)
            ->setError(true)
            ->setMessage($e->getMessage());
    }


    public function getSqlString($query): string
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }

    public function admin()
    {
        return Auth::guard('admin');
    }
}
