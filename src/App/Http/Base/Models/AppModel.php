<?php

namespace LoiPham\WooCommerce\App\Http\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use LoiPham\WooCommerce\App\Http\Base\Models\HasFactory as BaseHasFactory;
//use Illuminate\Support\Facades\DB;
//use LoiPham\WooCommerce\App\Observers\RecordFingerPrintObserver;
//use Illuminate\Support\Collection;

class AppModel extends Model
{
    use BaseHasFactory;

    public $table;
    public $tableClass;
    public $alias;
    protected $listQueryParams = [];

    public function setListQueryParams($params)
    {
        $this->listQueryParams = $params;
    }

    public function listItems($debug = false): ?\Illuminate\Database\Query\Builder
    {
        return $this->search('all', $this->listQueryParams, $debug);
    }

    public function getTableWithQueryBuilder($tableName = ''): ?\Illuminate\Database\Query\Builder
    {
        if (empty($tableName)) {
            return DB::table($this->table);
        } else {
            return DB::table($tableName);
        }
    }


    public function search($findType = 'all', $options = [], $debugSQL = false): ?\Illuminate\Database\Query\Builder
    {
        $table = $this->getTableWithQueryBuilder();

        if ($this->alias) {
            $table->from($this->table, $this->alias);
        } else {
            $table->from($this->table);
        }

        // Select Columns
        if (isset($options['columns'])) {
            $table->select($options['columns']);
        }

        // Set Conditions
        if (isset($options['where'])) {
            $where = $options['where'];

            if (is_array($where)) {
                if (count($where) > 0) {
                    foreach ($where as $value) {
                        $value = (array)$value;
                        switch ($value['compare']) {
                            case 'not_in':
                                $table->whereNotIn($value['column'], $value['value']);
                                break;
                            case 'in':
                                $table->whereIn($value['column'], $value['value']);
                                break;
                            case 'like':
                                $table->where($value['column'], $value['compare'], '%' . $value['value'] . '%');
                                break;
                            default:
                                if (!is_array($value['value'])) {
                                    $table->where($value['column'], $value['compare'], $value['value']);
                                }
                                break;
                        }
                    }
                }
            }
        }


        //Set Join;
        if (isset($options['joins']) and count($options['joins']) > 0) {
            foreach ($options['joins'] as $joinCondition) {
                $table->leftJoin($this->getTableNameByTableClass($joinCondition[0]), $this->getTableAlias($joinCondition[0]) . '.' . $joinCondition[1], '=', $this->getTableAlias($joinCondition[2]) . '.' . $joinCondition[3]);
            }
        }

        if (isset($options['orders'])) {
            $table->orderBy($options['orders'][0], $options['orders'][1]);
        } else {
            $table->orderBy('id', 'DESC');
        }

        if ($debugSQL) {
            dd($table->toSql());
        }
//        // Set Group By
//        if (isset($options['group'])) {
//            $select->group($options['group']);
//        }
//        // Set Having conditions
//        if (isset($options['having'])) {
//            $select->having($options['having']);
//        }

//
//        switch ($findType) {
//            case 'page':
//                $resultSetPrototype = new ResultSet();
//                if ($this->entity) {
//                    $resultSetPrototype->setArrayObjectPrototype(new $this->entity());
//                }
//                $paginatorAdapter = new DbSelect(
//                    $select,
//                    $this->tableGateway->getAdapter(),
//                    $resultSetPrototype
//                );
//                $paginator = new Paginator($paginatorAdapter);
//                return $paginator;
//                break;
//            case 'first':
//                $select->limit(1);
//                $table = $this->getTableGateway();
//                $rs = $table->selectWith($select);
//                $item = $rs->current();
//                if (isset($item->id)) {
//                    return $item;
//                } else {
//                    return null;
//                }
//                break;
//            case 'count':
//                $table = $this->getTableGateway();
//                $select->columns([new Expression('count(*) as count')]);
//                $rs = $table->selectWith($select);
//                return $rs->current()->count;
//                break;
//            default:
//                $table = $this->getTableGateway();
//                $rs = $table->selectWith($select);
//                return $rs;
//                break;
//        }
        return $table;
    }

    public function getTableNameByTableClass($tableClass = null): string
    {
        if (empty($tableClass)) {
            $table = new $this->tableClass;
        } else {
            $table = new $tableClass;
        }
        return $table->table . ' AS ' . $this->getTableAliasByTableClass($tableClass);
    }

    public function getTableAliasByTableClass($tableClass = null)
    {
        if (empty($tableClass)) {
            $table = new $this->tableClass;
        } else {
            $table = new $tableClass;
        }
        return $table->alias;
    }

    public function getTableAlias($tableClass = null)
    {
        if (empty($tableClass)) {
            $table = new $this->tableClass;
        } else {
            $table = new $tableClass;
        }
        return $table->alias;
    }

//    protected $primaryKeyField = 'id';
//    protected $option_order_field = 'name';
//    protected $tableClassName = null;
//    protected $listQueryParams = [];

//    protected $search_columns     = [];

//    public static function boot()
//    {
//        parent::boot();
//        parent::observe(new RecordFingerPrintObserver());
//    }

//    public function getOptions($id_field = null, $name_field = null, $wheres = [], $orders = []): \Illuminate\Support\Collection
//    {
//        if (!$id_field) $id_field = $this->primaryKeyField;
//        if (!$name_field) {
//            $name_field = $this->option_order_field;
//        }
//
//        $rs = DB::table($this->table);
//
//        if ($wheres) {
//
//            foreach ($wheres as $where) {
//                $rs = $rs->where("$where[0]", "$where[1]", "$where[2]");
//            }
//
//        }
//
//        if ($orders) {
//            $rs = $rs->orderBy("$orders[0]", "$orders[1]");
//        }
//
//        return $rs->get(["$id_field", "$name_field"]);
//    }

//    public function getSqlString($query): string
//    {
////        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
////            return is_numeric($binding) ? $binding : "'{$binding}'";
////        })->toArray());
//    }

//    public function getTable($tableName = null)
//    {
//
//        if (empty($tableName)) {
//            return DB::table($this->table);
//        } else {
//            return DB::table($tableName);
//        }
//    }

//    public function find($findType = 'all', $options = [], $debugSQL = false)
//    {
//        $table = $this->getTable();
//
//        // Select Columns
////        if (isset($options['columns'])) {
////            $select->columns($options['columns']);
////        }
//
//        // Set Conditions
//        if (isset($options['where'])) {
//            $where = $options['where'];
//
//            if (is_array($where)) {
//                if (count($where) > 0) {
//                    $table->where($where);
//                }
//            }
//        }
////        [['User' => TABLE_USERS], 'User.id = Assets.owner', ['owner_fullname' => 'fullname'], 'left']
////        $table->join('contacts', 'users.id', '=', 'contacts.user_id')->select('users.*', 'contacts.phone', 'orders.price');
////        var_dump($this->getSqlString($table));die;
//        //Set Join;
//        if (isset($options['joins']) and count($options['joins']) > 0) {
//            if (isset($options['joins']['inner']) and count($options['joins']['inner']) > 0) {
//                foreach ($options['joins']['inner'] as $inner) {
//                    if (count($inner) > 0) {
//                        $table->join($inner[0][0], $inner[0][1], $inner[0][2], $inner[0][3])->select($inner[1]);
//                    }
//                }
//            }
//
//            if (isset($options['joins']['left']) and count($options['joins']['left']) > 0) {
//                foreach ($options['joins']['left'] as $left) {
//                    if (count($left) > 0) {
//                        $table->leftJoin($left[0][0], $left[0][1], $left[0][2], $left[0][3])->select($left[1]);
//                    }
//                }
//            }
//
//            if (isset($options['joins']['right']) and count($options['joins']['right']) > 0) {
//                foreach ($options['joins']['right'] as $right) {
//                    if (count($right) > 0) {
//                        $table->rightJoin($right[0][0], $right[0][1], $right[0][2], $right[0][3])->select($right[1]);
//                    }
//                }
//            }
//            var_dump($this->getSqlString($table));
//            die;
//
//            foreach ($options['joins'] as $join) {
//                $table = $join[0];
//                $conditions = $join[1];
//                $fields = $join[2];
//                $type = isset($join[3]) ? $join[3] : 'inner';
//                $select->join($table, $conditions, $fields, $type);
//            }
//        }
//
//        // Set Group By
//        if (isset($options['group'])) {
//            $select->group($options['group']);
//        }
//        // Set Having conditions
//        if (isset($options['having'])) {
//            $select->having($options['having']);
//        }
//
//        //Set Sort order
//        if (isset($options['orders'])) {
//            $select->order($options['orders']);
//        }
//
//        if ($debugSQL) {
//            print($select->getSqlString($this->tableGateway->getAdapter()->getPlatform()));
//        }
//
//        switch ($findType) {
//            case 'page':
//                $resultSetPrototype = new ResultSet();
//                if ($this->entity) {
//                    $resultSetPrototype->setArrayObjectPrototype(new $this->entity());
//                }
//                $paginatorAdapter = new DbSelect(
//                    $select,
//                    $this->tableGateway->getAdapter(),
//                    $resultSetPrototype
//                );
//                $paginator = new Paginator($paginatorAdapter);
//                return $paginator;
//                break;
//            case 'first':
//                $select->limit(1);
//                $table = $this->getTableGateway();
//                $rs = $table->selectWith($select);
//                $item = $rs->current();
//                if (isset($item->id)) {
//                    return $item;
//                } else {
//                    return null;
//                }
//                break;
//            case 'count':
//                $table = $this->getTableGateway();
//                $select->columns([new Expression('count(*) as count')]);
//                $rs = $table->selectWith($select);
//                return $rs->current()->count;
//                break;
//            default:
//                $table = $this->getTableGateway();
//                $rs = $table->selectWith($select);
//                return $rs;
//                break;
//        }
//        return null;
//    }
//    /**
//     * Insert or update data
//     * @param $data
//     * @return array
//     * @throws \Exception
//     */
//    public function save($data) {
//        $data = (array)$data;
//        $pk   = $this->primaryKeyField;
//
//        if (isset($data[$pk])) {
//            $result       = $this->update($data, $data[$pk]);
//            $result['id'] = $data[$pk];
//        } else {
//            unset($data[$pk]);
//            $result = $this->insert($data);
//            if (!$result['error']) {
//                $result['id'] = $this->getTableGateway()->getLastInsertValue();
//            }
//        }
//        return $result;
//    }
}
