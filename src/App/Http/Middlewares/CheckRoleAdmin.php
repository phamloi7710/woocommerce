<?php

namespace LoiPham\WooCommerce\App\Http\Middlewares;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRoleAdmin
{
    public function handle($request, Closure $next)
    {
        if(Auth::guard('cms')->check())
        {
            return $next($request);
        }
        $notifySuccess = array(
            'message' => 'Không thể phân quyền',
            'alert-type' => 'error',
        );
        return redirect()->route('cms.admin.getLogin')->with($notifySuccess);
    }
}
