<?php

namespace LoiPham\WooCommerce\App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use LoiPham\WooCommerce\Commands\CreateSampleDataCommand;
use LoiPham\WooCommerce\Commands\Make\ControllerMakeCommand;
use LoiPham\WooCommerce\Commands\Make\MigrationMakeCommand;
use LoiPham\WooCommerce\Exceptions\Handler;
use LoiPham\WooCommerce\Traits\LoadAndPublishDataTrait;
use LoiPham\WooCommerce\Commands\SetupCommand;
use LoiPham\WooCommerce\Commands\CreateSuperUserCommand;
use Illuminate\Support\Facades\Schema;
use LoiPham\WooCommerce\Traits\SeederTrait;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Http\Kernel;
use LoiPham\WooCommerce\App\Http\Kernel as LoiPhamKernel;
use LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User;
use Illuminate\Console\Command;
use LoiPham\WooCommerce\Supports\Helper;
class WooCommerceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    use LoadAndPublishDataTrait, SeederTrait;

    public function register()
    {

        // Load All Configs
        $this->loadConfigs();
//        $router = $this->app['router'];
        //Khai bao middleware
//        $router->aliasMiddleware('checkRoleAdmin', CheckRoleAdmin::class);
        $this->loadAndActiveSingleton();
        Helper::autoload(__DIR__ . '/../../../helpers');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Load migrations
        $this->loadMigrations();

        $this->command();
        $this->loadAndActiveSeeder();
    }

    protected function loadAndActiveSeeder()
    {
        if ($this->app->runningInConsole()) {
            if (SeederTrait::isConsoleCommandContains('db:seed')) {
                SeederTrait::addSeedsAfterConsoleCommandFinished();
            }
        }
    }

    protected function command()
    {
        $this->commands([
            SetupCommand::class,
            ControllerMakeCommand::class,
            MigrationMakeCommand::class,
            CreateSuperUserCommand::class,
            CreateSampleDataCommand::class,
        ]);
    }

    /**
     * Load all config from cms module
     */
    private function loadConfigs()
    {
        // Cms
        $this->mergeConfigFrom(__DIR__ . '/../../../config/base/application.conf.php', 'application');
    }

    /**
     * Load all migration from cms module
     */
    private function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../Databases/Migrations');
    }

    private function loadAndActiveSingleton()
    {

        $this->app->singleton(
            Kernel::class,
            LoiPhamKernel::class
        );

//        $this->app->singleton(
//            ExceptionHandler::class,
//            Handler::class
//        );
    }
}
