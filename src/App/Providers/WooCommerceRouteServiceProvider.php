<?php

namespace LoiPham\WooCommerce\App\Providers;

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

class WooCommerceRouteServiceProvider extends RouteServiceProvider
{
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api/v1')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(__DIR__ . '/../../../routes/application.route.php');

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });
    }
}
