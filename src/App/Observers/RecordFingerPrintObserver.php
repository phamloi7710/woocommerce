<?php

namespace LoiPham\WooCommerce\App\Observers;

use Illuminate\Support\Facades\Auth;

class RecordFingerPrintObserver
{

    protected $userID;

    public function __construct()
    {
//        $this->userID = Auth::guard("cms")->id();
    }

    public function saving($model)
    {
//        $model->modfied_by = $this->userID;
    }

    public function saved($model)
    {
//        $model->modfied_by = $this->userID;
    }


    public function updating($model)
    {
//        $model->modfied_by = $this->userID;
    }

    public function updated($model)
    {
//        $model->modfied_by = $this->userID;
    }


    public function creating($model)
    {
        $model->created_by = $this->userID;
    }

    public function created($model)
    {
        $model->created_by = $this->userID;
    }


    public function removing($model)
    {
//        $model->purged_by = $this->userID;
    }

    public function removed($model)
    {
//        $model->purged_by = $this->userID;
    }
}
