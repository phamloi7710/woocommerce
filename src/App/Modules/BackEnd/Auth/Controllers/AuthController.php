<?php

namespace LoiPham\WooCommerce\App\Modules\BackEnd\Auth\Controllers;

use LoiPham\WooCommerce\App\Http\Requests\BackEnd\Auth\LoginRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use LoiPham\WooCommerce\App\Http\Base\Controllers\ManagerController;
use LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User;
use LoiPham\WooCommerce\App\Http\Responses\WooCommerceHttpResponse;

class AuthController extends ManagerController
{
    public function login(LoginRequest $request): WooCommerceHttpResponse
    {
        $user = User::where('username', $request->username)->first();
        if ($user == null) { // Tên đăng nhập không đúng
            $response = [
                'data' => null,
                'message' => [
                    'username' => [
                        'not_exists' => __('validation.not_exist', ['attribute' => __('validation.attributes.username')])
                    ]
                ],
                'code' => 401,
                'error' => true,
            ];
        } else {
            if (Hash::check($request->password, $user->password)) {
                $this->checkOldToken($user->id);
                $user['access_token'] = $user->createToken('AdminToken')->plainTextToken;
                $response = [
                    'data' => $user,
                    'message' => __('general.login_success'),
                    'code' => 200,
                    'error' => false
                ];
            } else {
                $response = [
                    'data' => [
                        'password' => [
                            'not_match' => __('validation.not_match', ['attribute' => __('validation.attributes.password')])
                        ]
                    ],
                    'message' => __('form.form_invalid'),
                    'code' => 401,
                    'error' => true,
                ];
            }
        }
        return $this->setApiResponse($response);
    }

    public function checkOldToken($currentUserId)
    {
        try {
            DB::beginTransaction();
            DB::table('personal_access_tokens')->where('tokenable_id', $currentUserId)->delete();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            $this->setApiResponseError($exception);
        }
    }

    public function logout()
    {
        try {
            $currentAdmin = $this->admin()->user();
            $currentAdmin->tokens()->delete();
            return $this->setApiResponseSuccess(null, __('logout_success'));
        } catch (\Exception $exception) {
            return $this->setApiResponseError($exception);
        }
    }
}
