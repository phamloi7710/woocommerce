<?php

namespace LoiPham\WooCommerce\App\Modules\BackEnd\Product\Models;

use LoiPham\WooCommerce\App\Http\Base\Models\AppModel;

class ProductModel extends AppModel
{
    public $table = 'products';
    public $alias = 'Products';

    public static function boot()
    {
        parent::boot();
    }
}
