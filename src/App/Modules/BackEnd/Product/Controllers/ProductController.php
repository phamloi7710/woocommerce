<?php

namespace LoiPham\WooCommerce\App\Modules\BackEnd\Product\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use LoiPham\WooCommerce\App\Http\Base\Controllers\ManagerController;
use LoiPham\WooCommerce\App\Modules\BackEnd\Product\Models\ProductModel;
use LoiPham\WooCommerce\App\Modules\BackEnd\User\Models\User;

class ProductController extends ManagerController
{
    public $tableClass = ProductModel::class;
    public $tableName = 'products';

    public function setListQueryParams($request)
    {
        $querySearch = $request->get('search');
        $querySort = $request->get('sort');
        $search = ($querySearch) ? json_decode(urldecode(base64_decode($querySearch))) : null;
        $sort = ($querySort) ? json_decode(urldecode(base64_decode($querySort))) : null;
        $where = [];
        if (!empty($search)) {
            foreach ($search as $value) {
                if (!empty($value->column)) {
                    switch ($value->column) {
                        case 'name':
                            $where[] = [
                                'column' => 'Products.name',
                                'compare' => $value->compare,
                                'value' => $value->value,
                            ];
                            break;
                        case 'product_id':
                            $where[] = [
                                'column' => 'Products.id',
                                'compare' => $value->compare,
                                'value' => $value->value,
                            ];
                            break;
                        default:
                            $where[] = $value;
                            break;
                    }
                }
            }
        }

        $joins = [
            [User::class, 'id', ProductModel::class, 'created_by'],
        ];

        // process search conditions
        $this->listQueryParams = [
            'columns' => [
                'Products.*',
                'Users.name as created_by_name'
            ],
            'joins' => $joins,
            'where' => $where,
            'orders' => isset($sort->column) ? [$sort->column, $sort->direction] : [
                'id', 'DESC'
            ]
        ];
    }

    public function allOptions(Request $request): JsonResponse
    {
        $table = $this->getTable();
        $userTable = $this->getTable('users');
        $data = [
            'products' => $table->get(['id', 'name']),
            'users' => $userTable->get(['id', 'name']),
        ];
        return $this->setResponse($data);
    }
}
